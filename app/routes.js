import { Router } from 'express';

import RootController from './controllers/root.controller';
import WebhookController from './controllers/webhook.controller';

import PostsController from './controllers/posts.controller';

import errorHandler from './middleware/error-handler';

const routes = new Router();

routes.get('/', RootController.index);
routes.get('/termsofservice', RootController.termsofservice);
routes.get('/privacypolicy', RootController.privacypolicy);

// Webhooks  for Facebook ChatBot
	// for verification of bot's access token to facebook api
routes.get('/webhook', WebhookController.verifyToken);
	// to post data
routes.post('/webhook', WebhookController.index);

// Post
routes.get('/posts', PostsController.search);
routes.post('/posts', PostsController.create);
routes.get('/posts/:id', PostsController._populate, PostsController.fetch);
routes.delete('/posts/:id', PostsController.delete);

routes.use(errorHandler);

export default routes;
