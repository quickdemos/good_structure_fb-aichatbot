import BaseController from './base.controller';

// access token as environmental var in .env
const token = process.env.PAGE_ACCESS_TOKEN;

class WebhookController extends BaseController {
  index(req, res) {
	  let messaging_events = req.body.entry[0].messaging;
	  console.log('messaging_events: '); console.log(messaging_events);

	  if (messaging_events) {
		  for (let i = 0; i < messaging_events.length; i++) {
			  let event = req.body.entry[0].messaging[i]
			  let sender = event.sender.id
			  if (event.message && event.message.text) {
				  let text = event.message.text
				  if (text === 'Generic'){
					  console.log("welcome to chatbot")
					  sendGenericMessage(sender)
					  continue
				  }
				  sendTextMessage(sender, "Text received, echo: " + text.substring(0, 200))
			  }
			  if (event.postback) {
				  let text = JSON.stringify(event.postback)
				  sendTextMessage(sender, "Postback received: "+text.substring(0, 200), token)
				  continue
			  }
		  }
	  }

	  res.sendStatus(200);
	}


  verifyToken(req, res) {
	  if (req.query['hub.verify_token'] === process.env.VERIFY_TOKEN) {
		  res.send(req.query['hub.challenge'])
	  } else {
		  res.send('Error, wrong token')
	  }
	}

}

function sendTextMessage(sender, text) {
	let messageData = { text:text };

	request({
		url: 'https://graph.facebook.com/v2.6/me/messages',
		qs: {access_token:token},
		method: 'POST',
		json: {
			recipient: {id:sender},
			message: messageData,
		}
	}, function(error, response, body) {
		if (error) {
			console.log('Error sending messages: ', error)
		} else if (response.body.error) {
			console.log('Error: ', response.body.error)
		}
	})
}

function sendGenericMessage(sender) {
	let messageData = {
		"attachment": {
			"type": "template",
			"payload": {
				"template_type": "generic",
				"elements": [{
					"title": "First card",
					"subtitle": "Element #1 of an hscroll",
					"image_url": "http://messengerdemo.parseapp.com/img/rift.png",
					"buttons": [{
						"type": "web_url",
						"url": "https://www.messenger.com",
						"title": "web url"
					}, {
						"type": "postback",
						"title": "Postback",
						"payload": "Payload for first element in a generic bubble",
					}],
				}, {
					"title": "Second card",
					"subtitle": "Element #2 of an hscroll",
					"image_url": "http://messengerdemo.parseapp.com/img/gearvr.png",
					"buttons": [{
						"type": "postback",
						"title": "Postback",
						"payload": "Payload for second element in a generic bubble",
					}],
				}]
			}
		}
	}
	request({
		url: 'https://graph.facebook.com/v2.6/me/messages',
		qs: {access_token:token},
		method: 'POST',
		json: {
			recipient: {id:sender},
			message: messageData,
		}
	}, function(error, response, body) {
		if (error) {
			console.log('Error sending messages: ', error)
		} else if (response.body.error) {
			console.log('Error: ', response.body.error)
		}
	})
}


export default new WebhookController();

