import BaseController from './base.controller';
import fs from 'fs';

class RootController extends BaseController {
  index(req, res) {
	  res.writeHead(200, {'Content-Type': 'text/html'});
	  fs.createReadStream('app/public/index.html').pipe(res);
	}

	// privacy policy and terms of service static files for facebook verification
  termsofservice(req, res) {
	  res.writeHead(200, {'Content-Type': 'text/html'});
	  fs.createReadStream('app/public/termsofservice.html').pipe(res);
	}
  privacypolicy(req, res) {
	  res.writeHead(200, {'Content-Type': 'text/html'});
	  fs.createReadStream('app/public/privacypolicy.html').pipe(res);
	}
}

export default new RootController();

